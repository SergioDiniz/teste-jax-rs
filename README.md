# Requisições a API
Usar um clientes REST como:
* Insomnia
* Postman

|Descrição |Verbo |Requisição |Body|
|-|-|-|-|
|Salvar Pessoa|`POST`|http://localhost:8080/apirest/pessoa/| { <br> "nome": "sergio diniz"  <br> } |
|Buscar Pessoa por ID|`GET`|http://localhost:8080/apirest/pessoa/1| |
|Listar Pessoas|`GET`|http://localhost:8080/apirest/pessoa/| |
|Deletar Pessoa por ID|`DELETE`|http://localhost:8080/apirest/pessoa/1| |
|Atualizar Pessoa|`PUT`|http://localhost:8080/apirest/pessoa/| {  <br>  "id": "1",	 <br> "nome": "sergio diniz 2"  <br> }|
