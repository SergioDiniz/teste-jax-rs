package api.handler;


import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;

//Usar em dev não vai exibir o erro
//@Provider
public class GenericExceptionHandler implements ExceptionMapper<Exception> {

    @Context
    private HttpServletRequest request;

    @Override
    public Response toResponse(Exception e) {

        ErrorResponse error = new ErrorResponse();
        Response.Status status = Response.Status.BAD_REQUEST;

        error.setError(status.getReasonPhrase());
        error.setStatus(status.getStatusCode());

        error.setMessage(e.getMessage());
        error.setPath(request.getRequestURI());
        error.setTimestamp(System.currentTimeMillis());
        error.setMetodo(request.getMethod());

        return Response
                .status(status)
                .entity(error)
                .build();

    }
}
