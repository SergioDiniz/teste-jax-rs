package api.handler;

import api.handler.excecoes.ArquivoNaoEncontradoException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.container.ResourceInfo;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;
import javax.ws.rs.ext.ExceptionMapper;

public class ArquivoNaoEncontradoExceptionHandler implements ExceptionMapper<ArquivoNaoEncontradoException> {

    @Context
    private HttpServletRequest request;

    @Context
    private HttpServletResponse response;

    @Context
    private ResourceInfo resourceInfo;

    @Context
    private UriInfo uriInfo;

    @Override
    public Response toResponse(ArquivoNaoEncontradoException e) {

        ErrorResponse error = new ErrorResponse();
        Response.Status status = Response.Status.NOT_FOUND;

        error.setError(status.getReasonPhrase());
        error.setStatus(status.getStatusCode());

        error.setMessage(e.getMessage());
        error.setPath(request.getRequestURI());
        error.setTimestamp(System.currentTimeMillis());
        error.setMetodo(request.getMethod());

        return Response
                .status(status)
                .entity(error)
                .build();

    }
}
