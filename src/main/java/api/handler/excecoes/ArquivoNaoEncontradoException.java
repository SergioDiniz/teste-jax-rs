package api.handler.excecoes;

import java.io.Serializable;

public class ArquivoNaoEncontradoException extends Exception implements Serializable {

    private static final long serialVersionUID = 1L;

    public ArquivoNaoEncontradoException() {
        super();
    }
    public ArquivoNaoEncontradoException(String msg)   {
        super(msg);
    }
    public ArquivoNaoEncontradoException(String msg, Exception e)  {
        super(msg, e);
    }

}
