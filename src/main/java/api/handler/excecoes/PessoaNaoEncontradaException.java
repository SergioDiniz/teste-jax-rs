package api.handler.excecoes;

import java.io.Serializable;

public class PessoaNaoEncontradaException extends Exception implements Serializable {

    private static final long serialVersionUID = 1L;

    public PessoaNaoEncontradaException() {
        super();
    }
    public PessoaNaoEncontradaException(String msg)   {
        super(msg);
    }
    public PessoaNaoEncontradaException(String msg, Exception e)  {
        super(msg, e);
    }
}
