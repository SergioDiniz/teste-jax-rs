package api.handler;

import api.handler.excecoes.PessoaNaoEncontradaException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.container.ResourceInfo;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;

@Provider
public class PessoaNaoEncontradaExceptionHandler implements ExceptionMapper<PessoaNaoEncontradaException> {

    @Context
    private HttpServletRequest request;

    @Context
    private HttpServletResponse response;

    @Context
    private ResourceInfo resourceInfo;

    @Context
    private UriInfo uriInfo;

    @Override
    public Response toResponse(PessoaNaoEncontradaException e) {

        ErrorResponse error = new ErrorResponse();
        Response.Status status = Response.Status.NOT_FOUND;

        error.setError(status.getReasonPhrase());
        error.setStatus(status.getStatusCode());

        error.setMessage(e.getMessage());
        error.setPath(request.getRequestURI());
        error.setTimestamp(System.currentTimeMillis());
        error.setMetodo(request.getMethod());

        return Response
                .status(status)
                .entity(error)
                .build();

    }
}
