package api.servicos;

import api.entidades.Pessoa;
import api.handler.excecoes.PessoaNaoEncontradaException;
import api.repositorios.PessoaRepositorio;

import javax.inject.Inject;
import java.util.Date;
import java.util.List;

public class PessoaService {

    @Inject
    private PessoaRepositorio pessoaRepositorio;

    public String getPessoaNome(){
        return "Sergio";
    }

    public Pessoa salvar(Pessoa pessoa){
        return pessoaRepositorio.salvarPessoa(pessoa);
    }

    public Pessoa salvarPessoa(){
        Pessoa p = new Pessoa("Sergio - ".concat(new Date().toString()));
        return pessoaRepositorio.salvarPessoa(p);
    }

    public List<Pessoa> listarPessoas(){
        return pessoaRepositorio.listarTodos();
    }

    public Pessoa buscarPessoaPorID(Long id) throws PessoaNaoEncontradaException {
        return pessoaRepositorio.buscarPessoaPorID(id);
    }

    public Pessoa deletarPessoaPorID(Long id) throws PessoaNaoEncontradaException {
        return pessoaRepositorio.deletarPessoaPorID(id);
    }

    public Pessoa atualizarPessoa(Pessoa pessoa) throws PessoaNaoEncontradaException {
        return pessoaRepositorio.atualizarPessoa(pessoa);
    }

}
