package api.servicos;

import api.DTO.ArquivoDTO;
import api.entidades.Arquivo;
import api.handler.excecoes.ArquivoNaoEncontradoException;
import api.repositorios.ArquivoRepositorio;
import org.apache.commons.codec.binary.Base64;

import javax.inject.Inject;
import java.util.ArrayList;
import java.util.List;

public class ArquivoService {

    @Inject
    private ArquivoRepositorio arquivoRepositorio;

    public ArquivoService() {
    }

    public List<Arquivo> salvarArquivos(List<Arquivo> arquivos){
        List<Arquivo> arquivosSalvos = new ArrayList<>();

        for (Arquivo arquivo: arquivos) {
            arquivosSalvos.add(arquivoRepositorio.salvarArquivo(arquivo));
        }

        return arquivosSalvos;
    }

    public ArquivoDTO buscarArquivoPorID(Long id) throws ArquivoNaoEncontradoException {

        Arquivo arquivo = arquivoRepositorio.buscarArquivoPorID(id);

        ArquivoDTO dto = new ArquivoDTO();

        dto.setArquivo(arquivo.getArquivo());
        dto.setArquivoId(arquivo.getId());
        dto.setPessoaID(arquivo.getPessoa().getId());
        dto.setArquivo64(
                "data:".concat(arquivo.getContentType())
                .concat(";base64,")
                .concat(new String(new Base64().encode(arquivo.getArquivo())))
        );

        return dto;
    }
}
