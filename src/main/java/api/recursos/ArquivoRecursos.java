package api.recursos;

import api.DTO.ArquivoDTO;
import api.entidades.Arquivo;
import api.entidades.Pessoa;
import api.handler.excecoes.ArquivoNaoEncontradoException;
import api.handler.excecoes.PessoaNaoEncontradaException;
import api.servicos.ArquivoService;
import api.servicos.PessoaService;
import com.google.gson.Gson;
import org.apache.commons.io.IOUtils;
import org.jboss.resteasy.plugins.providers.multipart.InputPart;
import org.jboss.resteasy.plugins.providers.multipart.MultipartFormDataInput;

import javax.inject.Inject;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.io.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@Path("/arquivo")
public class ArquivoRecursos {

    private PessoaService pessoaService;
    private ArquivoService arquivoService;

    public ArquivoRecursos() {
    }

    @Inject
    public ArquivoRecursos(PessoaService pessoaService, ArquivoService arquivoService) {
        this.pessoaService = pessoaService;
        this.arquivoService = arquivoService;
    }

    @POST
    @Path("/upload")
    @Consumes("multipart/form-data")
    @Produces(MediaType.APPLICATION_JSON)
    public Response uploadFile(MultipartFormDataInput input) throws PessoaNaoEncontradaException, IOException {
        List<Arquivo> arquivos = new ArrayList<>();

        Map<String, List<InputPart>> uploadForm = input.getFormDataMap();

        // Pegando pessoa que tambem foi enviada na request
        Pessoa pessoa = new Gson().fromJson(uploadForm.get("pessoa").get(0).getBodyAsString(), Pessoa.class);
        // Validando de pessoa (atendente) existe na base
        // Caso pessoa id não exista, será lançado PessoaNaoEncontradaException
        pessoa = pessoaService.buscarPessoaPorID(pessoa.getId());

        List<InputPart> inputParts = uploadForm.get("arquivo");
        // caso tenha mais de um arquivo
        for (InputPart inputPart : inputParts) {
            // Pegando bytes do arquivo para ser salvo na base como blob
            InputStream inputStream = inputPart.getBody(InputStream.class, null);
            byte[] bytes = IOUtils. toByteArray(inputStream);

            // Pegando informação do nome do arquivo.formato
            String nomeArquivo = inputPart.getHeaders().get("Content-Disposition").get(0).split("filename=")[1].split("\"")[1].trim().replaceAll("\"", "");
            nomeArquivo = (nomeArquivo != null && nomeArquivo.length() > 0) ? nomeArquivo : "nome_nao_informado";

            //Pegando informação do contentType do arquivo
            String contentType = inputPart.getMediaType().toString();

            Arquivo arquivo = new Arquivo();
            arquivo.setNomeArquivo(nomeArquivo);
            arquivo.setPessoa(pessoa);
            arquivo.setArquivo(bytes);
            arquivo.setContentType(contentType);
            arquivos.add(arquivo);
        }


        List<Arquivo> arquivosSalvos = arquivoService.salvarArquivos(arquivos);
        return Response.status(200).entity(arquivosSalvos).build();


    }


    // TODO retornar arquivo com informação do MIME types
    @GET
    @Path("/{id}")
    @Produces("application/json")
    public Response buscarArquivoPorID(@PathParam("id") Long id) throws ArquivoNaoEncontradoException {
        ArquivoDTO dto = arquivoService.buscarArquivoPorID(id);
        return Response.status(200).entity(dto).build();
    }


    @GET
    @Path("/{id}/abrir")
    @Produces("application/json")
    public Response abrirArquivoPorID(@PathParam("id") Long id) throws ArquivoNaoEncontradoException {
        ArquivoDTO dto = arquivoService.buscarArquivoPorID(id);
        return Response.status(200).entity(dto.getArquivo64()).build();
    }

}
