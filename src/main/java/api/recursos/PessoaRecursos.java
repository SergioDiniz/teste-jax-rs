package api.recursos;

import api.entidades.Pessoa;
import api.handler.excecoes.PessoaNaoEncontradaException;
import api.servicos.PessoaService;

import javax.inject.Inject;
import javax.json.Json;
import javax.ws.rs.*;
import javax.ws.rs.core.Response;

import java.util.List;

import static javax.ws.rs.core.MediaType.APPLICATION_JSON;

@Path("/pessoa")
@Produces(APPLICATION_JSON)
public class PessoaRecursos {

    @Inject
    private PessoaService pessoaService;


    @GET
    @Path("/")
    @Produces("application/json")
    public Response listasPessoas(){
        List<Pessoa> p = pessoaService.listarPessoas();
        return Response.ok(p).build();
    }

    @GET
    @Path("/{id}")
    @Produces("application/json")
    public Response pessoaPorID(@PathParam("id") Long id) throws PessoaNaoEncontradaException {
        Pessoa pessoa = pessoaService.buscarPessoaPorID(id);
        return Response.ok(pessoa).build();
    }

    @POST
    @Path("/")
    @Consumes("application/json")
    @Produces("application/json")
    public Response salvarPessoa(Pessoa pessoa){

        //aqui pode ser feito algumas validações, como se nome estar preenchido, ou tamanho minimo e etc.

        Pessoa p = pessoaService.salvar(pessoa);

        return Response
                .status(Response.Status.CREATED)
                .entity(p)
                .build();

    }

    @DELETE
    @Path("/{id}")
    @Produces("application/json")
    public Response deletarPessoaPorID(@PathParam("id") Long id) throws PessoaNaoEncontradaException {

        Pessoa p = pessoaService.deletarPessoaPorID(id);

        return Response
                .status(Response.Status.OK)
                .entity(p)
                .build();
    }

    @PUT
    @Path("/")
    @Consumes("application/json")
    @Produces("application/json")
    public Response atualizarPessoa(Pessoa pessoa) throws PessoaNaoEncontradaException {

        Pessoa p = pessoaService.atualizarPessoa(pessoa);

        return Response
                .status(Response.Status.OK)
                .entity(p)
                .build();

    }



    //
    // TESTES
    //

    @GET
    @Path("/teste")
    public Response  helloWorld(){
        return Response.ok(
                Json.createObjectBuilder()
                    .add("Resposta", "hello world")
                .build()
        ).build();
    }

    @GET
    @Path("/ola")
    public String getPessoaNome(){
        return "Ola, ".concat(pessoaService.getPessoaNome());
    }

    @GET
    @Path("/salvar")
    public Response salvarPessoa(){
        Pessoa p = pessoaService.salvarPessoa();
        return Response.ok(p).build();
    }




}
