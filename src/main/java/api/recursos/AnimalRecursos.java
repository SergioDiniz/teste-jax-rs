package api.recursos;

import api.DTO.PessoaAnimalDTO;
import api.entidades.Animal;
import api.entidades.Pessoa;
import com.google.gson.Gson;
import org.apache.commons.io.FileUtils;
import org.jboss.resteasy.plugins.providers.multipart.InputPart;
import org.jboss.resteasy.plugins.providers.multipart.MultipartFormDataInput;

import javax.json.Json;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import java.io.*;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

import static javax.ws.rs.core.MediaType.APPLICATION_JSON;

@Path("/animal")
@Produces(APPLICATION_JSON)
public class AnimalRecursos {


    /*
    * Exemplo de requisicao:
    {
        "pessoa": {
            "id": 1,
            "nome": "Sergio Diniz - From File"
        },
        "animal": {
            "id": 1,
            "nome": "Cachorro 2"
        }
    }
    * */
    @POST
    @Path("/")
    @Consumes("multipart/form-data")
    @Produces(MediaType.APPLICATION_JSON)
    public Response salvarDTO(MultipartFormDataInput input) throws IOException {

        Map<String, List<InputPart>> uploadForm = input.getFormDataMap();
        List<InputPart> inputParts = uploadForm.get("dto_file");

        if(inputParts.size() == 0){
            // significa que nenhum arquivo foi enviado
            // tratar lancando uma exception, por exemplo
        }

        // pega o primeiro pois so deveria ter 1 mesmo
        InputPart inputPart = inputParts.get(0);
        // converte o corpo da requisição em um input stream
        InputStream inputStream = inputPart.getBody(InputStream.class, null);
        // converte para reader, para poder ser lido pelo gson
        Reader reader = new InputStreamReader(inputStream);

        // convertendo o json que chegou por arquivo em DTO
        PessoaAnimalDTO dtoFromFile = new Gson().fromJson(reader, PessoaAnimalDTO.class);
        Pessoa p1 = new Pessoa();
        p1 = dtoFromFile.getPessoa();
        Animal a1 = new Animal();
        a1 = dtoFromFile.getAnimal();
        a1.setDono(p1);



        // convertendo o json que chegou pelo corpo da requisição em dto
        PessoaAnimalDTO dto = new Gson().fromJson(uploadForm.get("dto").get(0).getBodyAsString(), PessoaAnimalDTO.class);
        Pessoa p2 = new Pessoa();
        p2 = dto.getPessoa();
        Animal a2 = new Animal();
        a2 = dto.getAnimal();
        a2.setDono(p2);


        return Response.ok(Arrays.asList(a1, a2)).build();
    }

}
