package api.DTO;

import api.entidades.Animal;
import api.entidades.Pessoa;

public class PessoaAnimalDTO {

    private Pessoa pessoa;
    private Animal animal;

    public PessoaAnimalDTO() {
    }

    public PessoaAnimalDTO(Pessoa pessoa, Animal animal) {
        this.pessoa = pessoa;
        this.animal = animal;
    }

    public Pessoa getPessoa() {
        return pessoa;
    }

    public void setPessoa(Pessoa pessoa) {
        this.pessoa = pessoa;
    }

    public Animal getAnimal() {
        return animal;
    }

    public void setAnimal(Animal animal) {
        this.animal = animal;
    }
}
