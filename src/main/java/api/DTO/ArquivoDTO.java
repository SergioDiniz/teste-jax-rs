package api.DTO;

import java.io.Serializable;

public class ArquivoDTO implements Serializable {

    private byte[] arquivo;
    private String arquivo64;
    private Long arquivoId;
    private Long pessoaID;

    public ArquivoDTO() {
    }

    public byte[] getArquivo() {
        return arquivo;
    }

    public void setArquivo(byte[] arquivo) {
        this.arquivo = arquivo;
    }

    public Long getArquivoId() {
        return arquivoId;
    }

    public void setArquivoId(Long arquivoId) {
        this.arquivoId = arquivoId;
    }

    public Long getPessoaID() {
        return pessoaID;
    }

    public void setPessoaID(Long pessoaID) {
        this.pessoaID = pessoaID;
    }

    public String getArquivo64() {
        return arquivo64;
    }

    public void setArquivo64(String arquivo64) {
        this.arquivo64 = arquivo64;
    }
}
