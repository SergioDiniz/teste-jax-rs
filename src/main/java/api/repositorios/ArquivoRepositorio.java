package api.repositorios;

import api.entidades.Arquivo;
import api.handler.excecoes.ArquivoNaoEncontradoException;
import api.utils.JPAUtils;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.List;

@Transactional
public class ArquivoRepositorio {

    EntityManagerFactory emf = JPAUtils.getEntityManagerFactory(); //persistence-unit name <persistence.xml>
    EntityManager factory = emf.createEntityManager();

    public Arquivo salvarArquivo(Arquivo arquivo) {
        try {
            this.factory = emf.createEntityManager();
            this.factory.persist(arquivo);
            this.factory.flush();
        } catch (Exception e) {
            e.getMessage();
        } finally {
            if (this.factory.isOpen()) {
                this.factory.close();
            }
        }

        return arquivo;
    }


    public Arquivo buscarArquivoPorID(Long id) throws ArquivoNaoEncontradoException {

        if(id == null){
            throw new ArquivoNaoEncontradoException(String.format("Não foi possível encontra o arquivo com id nulo"));
        }

        Arquivo arquivo = new Arquivo();

        try {

            this.factory = emf.createEntityManager();
            arquivo = this.factory.find(Arquivo.class, id);
            arquivo.getPessoa();

        } catch (Exception e) {
            e.getMessage();
        } finally {
            if(this.factory.isOpen()){
                this.factory.close();
            }
        }

        if(arquivo == null){
            throw new ArquivoNaoEncontradoException(String.format("Não foi possível encontra pessoa: %s", id));
        }

        return arquivo;

    }

}
