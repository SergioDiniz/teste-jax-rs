package api.repositorios;

import api.entidades.Pessoa;
import api.handler.excecoes.PessoaNaoEncontradaException;
import api.utils.JPAUtils;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Query;
import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.List;

@Transactional
public class PessoaRepositorio {

    EntityManagerFactory emf = JPAUtils.getEntityManagerFactory(); //persistence-unit name <persistence.xml>
    EntityManager factory = emf.createEntityManager();

    public Pessoa salvarPessoa(Pessoa pessoa){

        try {
            this.factory = emf.createEntityManager();
            if (pessoa.getId() == null) {
                this.factory.persist(pessoa);
                this.factory.flush();
            } else {
                pessoa = this.factory.merge(pessoa);
                this.factory.flush();
            }
        } catch (Exception e) {
            e.getMessage();
        } finally {
            if(this.factory.isOpen()){
                this.factory.close();
            }
        }


        return pessoa;

    }

    public List<Pessoa> listarTodos(){

        List<Pessoa> pessoas = new ArrayList<Pessoa>();

        try {

            this.factory = emf.createEntityManager();

            Query query = this.factory.createQuery("select p from Pessoa p order by p.id desc");
            //query.setParameter("parametro1", parametro1);

            pessoas = query.getResultList();
        } catch (Exception e) {

            e.getMessage();
            //this.factory.getTransaction().rollback();

        } finally {
            if(this.factory.isOpen()){
                this.factory.close();
            }
        }


        return pessoas;

    }

    public Pessoa buscarPessoaPorID(Long id) throws PessoaNaoEncontradaException {

        if(id == null){
            throw new PessoaNaoEncontradaException(String.format("Não foi possível encontra pessoa com id nulo"));
        }

        Pessoa pessoa = new Pessoa();

        try {

            this.factory = emf.createEntityManager();
            pessoa = this.factory.find(Pessoa.class, id);

        } catch (Exception e) {
            e.getMessage();
        } finally {
            if(this.factory.isOpen()){
                this.factory.close();
            }
        }

        if(pessoa == null){
            throw new PessoaNaoEncontradaException(String.format("Não foi possível encontra pessoa: %s", id));
        }

        return pessoa;

    }

    public Pessoa deletarPessoaPorID(Long id) throws PessoaNaoEncontradaException {
        // Buscando pessoa
        // metodo buscarPessoaPorID ja valida se pessoa existe
        Pessoa pessoa = buscarPessoaPorID(id);

        try {
            this.factory = emf.createEntityManager();

            // this.factory.merge necessario para tornar instancia de pessoa gerenciada pelo contexto ("managed"),
            // caso contrario erro de entidate "detached" ocorre.
            this.factory.remove(this.factory.merge(pessoa));

        } catch (Exception e) {
            e.getMessage();
        } finally {
            if(this.factory.isOpen()){
                this.factory.close();
            }
        }

        return pessoa;

    }

    public Pessoa atualizarPessoa(Pessoa pessoa) throws PessoaNaoEncontradaException {
        // busca pessoa para verificar se id pessoa existe
        // caso id nao exista PessoaNaoEncontradaException eh lancada e processo acaba aqui
        buscarPessoaPorID(pessoa.getId());

        return salvarPessoa(pessoa);

    }

}
